<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
        public function __construct()
	{
            parent::__construct();
            $this->load->helper(array('form','url'));
            $this->load->library(array('session', 'form_validation'));
            $this->load->database();
            $this->load->model('user_model');
	}
	
	function index()
	{
            // set form validation rules
            $this->form_validation->set_rules('fname','First Name','trim|required');
            $this->form_validation->set_rules('lname','Last Name','trim|required');
            $this->form_validation->set_rules('email','Email ID','trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|md5');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]|md5');
            // submit
            if ($this->form_validation->run() == FALSE)
            {
                    // fails
                if ($this->session->userdata('login'))
                    redirect('profile/index');
                else
                    $this->load->view('home_view');
            }
            else
            {
                    //insert user details into db
                    $data = array(
                            'fname' => $this->input->post('fname'),
                            'lname' => $this->input->post('lname'),
                            'email' => $this->input->post('email'),
                            'password' => $this->input->post('password')
                    );

                    if ($this->user_model->insert_user($data))
                    {
                            mkdir('uploads/'.str_replace('.', '', $data['email']));
                            $this->session->set_flashdata('msg','<div class="alert alert-success text-center">You are Successfully Registered '.$name.$base.'! Please login to access your Profile!</div>');
                            redirect('welcome/index');
                    }
                    else
                    {
                            // error
                            $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                            redirect('welcome/index');
                    }
            }
	}
}
