<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author Stefan-Alexandru
 */
class Login extends CI_Controller {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper(array('form','url','html','cookie'));
            $this->load->library(array('session', 'form_validation'));
            $this->load->database();
            $this->load->model('user_model');
        }

        function index()
        {
            // get form input
            $email = $this->input->post("email");
            $password = $this->input->post("password");

            // form validation
            $this->form_validation->set_rules('email','Email-ID','trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                // validation fail
                if ($this->session->userdata('login')||$this->input->cookie('email'))
                    redirect('profile/index');
                else
                    $this->load->view('login_view');
            }
            else
            {
                // set cookie
                if($this->input->post('rememberme')!='')
                {
                    $cookie = array(
                    'name'   => 'email',
                    'value'  => $email,
                    'expire' =>  86500,
                    'secure' => false
                );
                $this->input->set_cookie($cookie);
                
                // check for user credentials
                $uresult = $this->user_model->get_user($email, $password);
                if (count($uresult) > 0)
                {
                    // set session
                    $sess_data = array(
                                       'login' => TRUE,
                                       'uname' => $uresult[0]->fname,
                                       'uid' => $uresult[0]->user_id,
                                       //strip uemail to use as folder name
                                       'uemail' => str_replace('.','',$uresult[0]->email)
                                       );
                    $this->session->set_userdata($sess_data);
                    redirect("profile/index");
                }
                else
                {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email-ID or Password!</div>');
                    redirect('login/index');
                }
            }
        }
    }
}
