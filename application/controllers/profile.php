<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of profile
 *
 * @author pauls
 */
class Profile extends CI_Controller {
      
        public function __construct()
        {
            parent::__construct();
            $this->load->helper(array('form','url','cookie'));
            $this->load->library(array('session', 'form_validation'));
            $this->load->database();
            $this->load->model('user_model');
            $this->load->model('file_model');
        }

        function index()
        {
            if ($this->session->userdata('login')||$this->input->cookie('email')){
                $data['query'] = $this->file_model->get_rows($this->session->userdata('uid'));
                $this->load->view('profile_view',$data);
            }
            else {
                redirect(welcome/index);
            }
        }

        function do_upload(){
            if ($_FILES['file'] != "") {

                //load library
                $this->load->library('upload');

                //Set the config
                $config['upload_path'] = 'uploads/'.$this->session->userdata('uemail'); 
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['overwrite'] = FALSE; //If the file exists it will be saved with a progressive number appended

                //Initialize
                $this->upload->initialize($config);

                //Upload file
                if (!$this->upload->do_upload('file')) {
                    //echo the errors
                    echo $this->upload->display_errors();
                }

                //If the upload success
                $full_path=$this->upload->data('full_path');
                
                $name = pathinfo($this->upload->data('file_name'),PATHINFO_FILENAME);
                /*size in MB(1024*1024 B) with a precision of 3
                 *'@' operator suppresses error messages 
                 */
                $size=round(@filesize($full_path)/1048576,2);
                //extension of the file
                $ext = pathinfo($full_path, PATHINFO_EXTENSION);
                //modification time at uploading
                $modif_time=date('Y-m-d H:i:s', @filemtime($full_path));
                $uid=$this->session->userdata('uid');
             
                //save file properties into DB
                $props = array(
                        'file_name' => $name,
                        'file_ext' => $ext,
                        'file_time' => $modif_time,
                        'file_size' => $size,
                        'user_id' => $uid
                );

                $this->file_model->insert_file($props);
                redirect("profile/index");
              
            }
            else {
                echo 'Wrong path or file does not exists!';
            }
        }
        function delete($file_id)
        {
            $res = $this->file_model->get_file_by_id($file_id);
            $file_path='uploads/'.$this->session->userdata('uemail').'/'.$res[0]->file_name.'.'.$res[0]->file_ext;
            if (file_exists($file_path)) {
  			// for files
  			if (is_file($file_path)) {
  				unlink($file_path);
                        }
                        $this->file_model->delete_file($file_id);
  			redirect("profile/index");
            }
            else {
                echo 'Wrong path or file does not exists!';
            }
        }

        function logout()
        {
            // destroy session
            $data = array('login' => '', 'uname' => '', 'uid' => '', 'uemail' => '');
            $this->session->unset_userdata($data);
            $this->session->sess_destroy();
            delete_cookie('email');
            redirect('welcome/index');
        }
}