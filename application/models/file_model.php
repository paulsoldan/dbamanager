<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of file_model
 *
 * @author Stefan-Alexandru
 */
class file_model extends CI_Model{
    
	function __construct()
        {
            parent::__construct();
        }
        
        function get_rows($uid)
        {
            $this->db->where('user_id', $uid);
            $query = $this->db->get('files');
            return $query->result();
        }
	
        function get_file_by_id($id)
        {
            $this->db->where('file_id', $id);
            $query = $this->db->get('files');
            return $query->result();
        }

        // insert
	function insert_file($data)
        {
            return $this->db->insert('files', $data);
	}
        
        function delete_file($id)
        {
            $this->db->where('file_id', $id);
            return $this->db->delete('files');
        }
}
