<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model
{
	function __construct()
        {
            parent::__construct();
        }
	
	function get_user($email, $pwd)
	{
            $this->db->where('email', $email);
            $this->db->where('password', md5($pwd));
            $query = $this->db->get('users');
            return $query->result();
	}
	
	// get user
	function get_user_by_id($id)
	{
            $this->db->where('user_id', $id);
            $query = $this->db->get('users');
            return $query->result();
	}
        
        function get_user_id_by_email($email)
	{
            $this->db->where('email', $email);
            $query = $this->db->get('users');
            return $query->result();
	}
	
	// insert
	function insert_user($data)
        {
            return $this->db->insert('users', $data);
	}
}?>