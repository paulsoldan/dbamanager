<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Files Manager</title>
        <link rel="icon" href="<?php echo $baseurl=base_url();?>assets/img/folder.png">
        <link rel="stylesheet" href="<?php echo $baseurl=base_url();?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $baseurl=base_url();?>assets/css/ChooseAFile.css">
        <link rel="stylesheet" href="<?php echo $baseurl=base_url();?>assets/css/socialMedia.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url(); ?>welcome/index">Files Manager</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right social">
				<?php if ($this->session->userdata('login')){ ?>
				<li><p class="navbar-text">Hello <b><?php echo $this->session->userdata('uname'); ?></b></p></li>
				<li><a href="<?php echo base_url(); ?>profile/logout">Log Out</a></li>
				<?php } else { ?>
				<li><a href="https://facebook.com"><i class="fa fa-lg fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/"><i class="fa fa-lg fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com"><i class="fa fa-lg fa-google-plus"></i></a></li>
                                <li><a href="https://www.linkedin.com"><i class="fa fa-lg fa-linkedin"></i></a></li>
				<li><a href="<?php echo base_url(); ?>profile/logout">Log Out</a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</nav>
<br/>
<?php echo form_open_multipart(base_url().'profile/do_upload');?>
<div class="container">
  		<div class="text-center">
  			<p><h2>Select file:</h2></p>
  			<center>
  				<div class="form-group">
  					<input type="file" name="file" id="file" class="input-file">
  					<label for="file" class="btn btn-tertiary js-labelFile">
    					<i class="icon fa fa-check"></i>
   						 <span class="js-fileName">Choose a file</span>
  					</label>
				</div>
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                                <script type="text/javascript" src="<?php echo $baseurl=base_url();?>assets/js/ChooseAFile.js"></script>
        	</center>
  		</div>
</div>
<div class="container">
  		<div class="row" style="text-align:center;color: white;font-weight:300;margin-bottom:30px;">
  			<center>
                            <input type="submit" value="upload" class="btn btn-large outline btn-danger radial-out"/>
  			</center>
  		</div>
</div>
<!-- Uploaded file preview -->
	<div>
	    <embed id="UploadedFile" src="" width="390px" height="16px">
	</div>
	<div class="table-reponsive">
		<table class="table">
			<thead>
				<tr>
					<th><h3>Filename</h3></th>
					<th><h3>Type</h3></th>
					<th><h3>Size <small>(MB)</small></h3></th>
					<th><h3>Date Modified</h3></th>
					<th><h3>Delete</h3></th>
				</tr>
			</thead>
                        <tbody>
                            <?php foreach($query as $row): ?>
                            <tr>   
                                <td>
                                    <a href='<?php echo $baseurl=base_url();?>uploads/<?php echo $this->session->userdata('uemail').'/'.$row->file_name.".".$row->file_ext; ?>'
                                       download><?php echo $row->file_name; ?>
                                    </a>
                                </td>
                                <td><?php echo $row->file_ext; ?></td>
                                <td><?php echo $row->file_size; ?></td>
                                <td><?php echo $row->file_time; ?></td>
                                <td>
                                    <a href='<?php echo base_url(); ?>profile/delete/<?php echo $row->file_id; ?>'>
                                        <img src='<?php echo $baseurl=base_url();?>/assets/img/x.png' style='width: 50px; height: 50px;'/>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
  	</div>
<script type="text/javascript" src="<?php echo $baseurl=base_url();?>assets/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo $baseurl=base_url();?>assets/js/bootstrap.js"></script>
</body>
</html>
